# Code in this depecrated repo has been released to the public domain
_ID := jtag

_STAMP := $(STAMPS_DIR)/$(_ID)
_STAMP_COPY := $(STAMPS_DIR)/$(_ID)-copy

PREP_STAMPS += $(_STAMP)

.PHONY: $(_ID) $(_ID)_copy
$(_ID): $(_STAMP)
$(_ID)-copy: $(_STAMP_COPY)


_SRC_IN_DIR := $(TOPDIR)/modules/c4m-jtag/rtl/vhdl
_SRC_OUT_DIR := $(SRC_OUT_DIR)/$(_ID)

_COPY_SRC_FILES := \
  c4m_jtag_pkg.vhdl \
  c4m_jtag_idblock.vhdl \
  c4m_jtag_iocell.vhdl \
  c4m_jtag_ioblock.vhdl \
  c4m_jtag_irblock.vhdl \
  c4m_jtag_tap_fsm.vhdl \
  c4m_jtag_tap_controller.vhdl
_COPY_SRC_OUT := $(addprefix $(_SRC_OUT_DIR)/,$(_COPY_SRC_FILES))
_COPY_SRC_IN := $(addprefix $(_SRC_IN_DIR)/,$(_COPY_SRC_FILES))

_NMIGEN_IN_DIR := $(TOPDIR)/modules/c4m-jtag/rtl/nmigen
_COPY_NMIGEN_OUT := $(NMIGEN_OUT_DIR)/$(_ID)/jtag.py
_COPY_NMIGEN_IN := $(_NMIGEN_IN_DIR)/jtag.py

_PATCH := $(PATCHES_DIR)/$(_ID).diff

$(_STAMP): PATCH := $(_PATCH)
$(_STAMP): $(_STAMP_COPY)
	@echo "jtag: patching files"
	@patch -s -p1 < $(PATCH)
	@touch $@


$(_STAMP_COPY): SRC_FILES := $(_COPY_SRC_FILES)
$(_STAMP_COPY): SRC_IN_DIR := $(_SRC_IN_DIR)
$(_STAMP_COPY): SRC_OUT_DIR := $(_SRC_OUT_DIR)
$(_STAMP_COPY): NMIGEN_IN := $(_COPY_NMIGEN_IN)
$(_STAMP_COPY): NMIGEN_OUT := $(_COPY_NMIGEN_OUT)
# Also copy files if patch has changed so new platch applies cleanly
$(_STAMP_COPY): $(_COPY_SRC_IN) $(_COPY_NMIGEN_IN) $(_PATCH) | $(_SRC_OUT_DIR)
	@echo "jtag: copying files"
	@for f in $(SRC_FILES); do cp $(SRC_IN_DIR)/$$f $(SRC_OUT_DIR)/$$f; done
	@cp $(NMIGEN_IN) $(NMIGEN_OUT)
	@touch $@

$(_SRC_OUT_DIR):
	@mkdir -p $@


.PHONY: $(_ID)-clean
clean:: $(_ID)-clean
$(_ID)-clean: FILES:=$(_SRC_OUT_DIR) $(_COPY_NMIGEN_OUT) $(_STAMP) $(_STAMP_COPY)
$(_ID)-clean:: 
	@rm -fr $(FILES)
