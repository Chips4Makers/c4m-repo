# Code in this depecrated repo has been released to the public domain
_ID := lib

_STAMP := $(STAMPS_DIR)/$(_ID)
_STAMP_COPY := $(STAMPS_DIR)/$(_ID)-copy

PREP_STAMPS += $(_STAMP)

.PHONY: $(_ID) $(_ID)_copy
$(_ID): $(_STAMP)
$(_ID)-copy: $(_STAMP_COPY)

_COPY_NMIGEN_FILES := wishbone.py plat.py
_COPY_NMIGEN_OUT_DIR := $(NMIGEN_OUT_DIR)/$(_ID)
_COPY_NMIGEN_OUT := $(addprefix $(_COPY_NMIGEN_OUT_DIR)/,$(_COPY_NMIGEN_FILES))
_COPY_NMIGEN_IN_DIR := $(TOPDIR)/modules/c4m-lib/rtl/nmigen/

$(_STAMP): $(_STAMP_COPY)
	@touch $@


$(_STAMP_COPY): $(_COPY_NMIGEN_OUT)
	@echo "c4m-lib: files copied"
	@touch $@

$(_COPY_NMIGEN_OUT): $(_COPY_NMIGEN_OUT_DIR)/% : $(_COPY_NMIGEN_IN_DIR)/%
	@cp $< $@


.PHONY: $(_ID)-clean
clean:: $(_ID)-clean
$(_ID)-clean: FILES := $(COPY_NMIGEN_OUT) $(_STAMP) $(_STAMP_COPY)
$(_ID)-clean:
	@rm -fr $(FILES)
