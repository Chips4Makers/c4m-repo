TOPDIR := $(realpath .)

.PHONY: help
help:
	@echo
	@echo "You can use the following targets:"
	@echo "  make prep: copy and prepare files from submodules"
	@echo "  make install: install the repo; it will use pip for installing python code."
	@echo "For more documentation use the source inside the Makefile"
	@echo

SRC_OUT_DIR := $(TOPDIR)/c4m_repo/src
PATCHES_DIR := $(TOPDIR)/patches
STAMPS_DIR := $(TOPDIR)/.stamps
MIGEN_OUT_DIR := $(TOPDIR)/c4m_repo/migen
NMIGEN_OUT_DIR := $(TOPDIR)/c4m_repo/nmigen

BASE_STAMP := $(STAMPS_DIR)/base
PREP_STAMPS := $(BASE_STAMP)
# Include makefiles for each module.
# These makefiles have to add their stamp(s) to the STAMPS makefile variable.
# The names are sorted so order of inclusion is deterministic.
MAKE_INCLUDES := $(sort $(wildcard makefiles/*.mk))
include $(MAKE_INCLUDES)

.PHONY: prep
$(PREP_STAMPS): | $(STAMPS_DIR)
prep: $(PREP_STAMPS)

$(BASE_STAMP): | $(SRC_OUT_DIR)
	@touch $@

$(STAMPS_DIR) $(SRC_OUT_DIR):
	@mkdir -p $@


clean::
	@rm -f $(BASE_STAMP)


# pip command and arguments can be configured by make PIP=... PIP_ARGS=...
PIP ?= pip
PIP_ARGS ?=
.PHONY: install
install: prep
	@echo "Installing repo"
	@$(PIP) $(PIP_ARGS) install .
